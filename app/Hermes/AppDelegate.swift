//
//  AppDelegate.swift
//  Hermes
//
//  Created by James Hoffman on 2016-07-12.
//  Copyright © 2016 jhoffman.ca. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
}

