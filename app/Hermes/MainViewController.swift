//
//  MainViewController.swift
//  Hermes
//
//  Created by James Hoffman on 2016-07-12.
//  Copyright © 2016 jhoffman.ca. All rights reserved.
//

import Cocoa
import WebKit

class MainViewController: NSViewController, WebUIDelegate, WebPolicyDelegate {
    
    private lazy var __once: () = {
            let window = self.view.window!
            
            window.title = ""
            window.setContentSize(self.MIN_WINDOW_SIZE)
            window.contentMinSize = self.MIN_WINDOW_SIZE
            
            self.loadHomeRequest()
        }()
    
    //
    //MARK: Constants
    //
    
    fileprivate let MAIN_FRAME_TITLE_KEY = "mainFrameTitle"
    fileprivate let MESSENGER_URL = URL(string: "https://www.messenger.com/")!
    fileprivate let MIN_WINDOW_SIZE = NSSize(width: 1105, height: 735)
    
    //
    //MARK: IBOutlets
    //
    
    @IBOutlet weak var webView: WebView!
    
    //
    //MARK: Members
    //
    
    fileprivate var mainFrameTitleObserverContext = 0
    fileprivate var onceToken = 0
    
    //
    //MARK: View Controller
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.addObserver(self, forKeyPath: MAIN_FRAME_TITLE_KEY, options: .new, context: &mainFrameTitleObserverContext)
        webView.uiDelegate = self
        webView.policyDelegate = self
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()

        _ = self.__once
    }
    
    deinit {
        webView.removeObserver(self, forKeyPath: MAIN_FRAME_TITLE_KEY, context: &mainFrameTitleObserverContext)
    }

    //
    //MARK: Key-Value Observer
    //
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &mainFrameTitleObserverContext {
            if let newTitle = change?[NSKeyValueChangeKey.newKey] as? String {
                self.view.window?.title = newTitle
            
                updateNotificationBadge(newTitle)
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    //
    //MARK: MainViewController
    //
    
    func updateNotificationBadge(_ titleString: String) {
        let pattern = "\\(\\d+\\)"
        let currentBadge = NSApplication.shared().dockTile.badgeLabel
        var badge: String? = titleString == "Messenger" ? nil : currentBadge
        
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let fullRange = NSMakeRange(0, titleString.characters.distance(from: titleString.startIndex, to: titleString.endIndex))
            if let result = regex.firstMatch(in: titleString, options: .reportCompletion, range: fullRange) {
                let range = result.range
                let startIndex = range.location + 1 // remove first (
                let endIndex = range.length - 2 //-2 to remove both ( and )à
                let numberRange = NSMakeRange(startIndex, endIndex)
                let numberSubstring = (titleString as NSString).substring(with: numberRange)
                
                badge = numberSubstring
            }
        } catch {
            badge = nil
        }
        
        NSApplication.shared().dockTile.badgeLabel = badge
    }
    
    fileprivate func loadHomeRequest() {
        self.webView.mainFrame.load(URLRequest(url: self.MESSENGER_URL))
    }
    
    fileprivate func loadNewMessageRequest() {
        let url = self.MESSENGER_URL.appendingPathComponent("new")
        self.webView.mainFrame.load(URLRequest(url: url))
    }
    
    func onHomeClicked() {
        loadHomeRequest()
    }
    
    func onNewMessageClicked() {
        loadNewMessageRequest()
    }
    
    func onBackClicked() {
        webView.goBack()
    }
    
    func onForwardClicked() {
        webView.goForward()
    }
    
    func onRefreshClicked() {
        webView.reload(nil)
    }
    
    //
    //MARK: WebUIDelegate
    //
    
    func webView(_ sender: WebView!, runOpenPanelForFileButtonWith resultListener: WebOpenPanelResultListener!) {
        let openPanel = NSOpenPanel()
        openPanel.canChooseFiles = true
        openPanel.canChooseDirectories = false
        openPanel.allowsMultipleSelection = false
        
        openPanel.beginSheetModal(for: self.view.window!) { panelResult in
            if (panelResult == NSModalResponseOK) {
                if let filename = openPanel.urls.first?.relativePath {
                    resultListener.chooseFilename(filename)
                }
            }
        }
    }
    
    func webView(_ sender: WebView!, createWebViewWith request: URLRequest!) -> WebView! {
        return self.webView;
    }
    
    //
    //MARK: WebPolicyDelegate
    //
    
    func webView(_ webView: WebView!, decidePolicyForNavigationAction actionInformation: [AnyHashable : Any]!, request: URLRequest!, frame: WebFrame!, decisionListener listener: WebPolicyDecisionListener!) {
        
        if let url = request.url {
            let urlString = url.absoluteString
            if (urlString.range(of: "(https:\\/\\/www\\.messenger\\.com)|(about:blank)",
                                options: String.CompareOptions.regularExpression,
                                range: urlString.startIndex..<urlString.endIndex, locale: nil) != nil) {
                listener.use()
            } else {
                listener.ignore()
                NSWorkspace.shared().open(url)
            }
        }
    }
}

