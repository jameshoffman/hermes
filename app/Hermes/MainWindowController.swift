//
//  MainWindowController.swift
//  Hermes
//
//  Created by James Hoffman on 2016-07-14.
//  Copyright © 2016 jhoffman.ca. All rights reserved.
//

import Foundation
import Cocoa

class MainWindowController: NSWindowController {
    
    fileprivate var mainViewController: MainViewController {
        return contentViewController as! MainViewController
    }
    
    //
    //MARK: MainWindowController
    //
    
    override func windowDidLoad() {
        super.windowDidLoad()

        window?.setFrameOrigin(NSPoint(x: 0, y: 0))
    }
    
    //
    //MARK: IBActions
    //
    
    @IBAction func onHomeClicked(_ sender: AnyObject) {
        mainViewController.onHomeClicked()
    }

    @IBAction func onNewMessageClicked(_ sender: AnyObject) {
        mainViewController.onNewMessageClicked()
    }
    
    @IBAction func onBackClicked(_ sender: AnyObject) {
        mainViewController.onBackClicked()
    }
    
    @IBAction func onForwardClicked(_ sender: AnyObject) {
        mainViewController.onForwardClicked()
    }
    
    @IBAction func onRefreshClicked(_ sender: AnyObject) {
        mainViewController.onRefreshClicked()
    }
}
